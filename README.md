# Clonar Repositorio de la API
git clone https://cavendano43@bitbucket.org/cavendano43/fz-sport-api.git -b Desarrollo

# Instalar API
npm i

# Correr API
comando npm run dev

# Clonar Repositorio del FRONTEND
git clone https://cavendano43@bitbucket.org/cavendano43/fz-test.git -b Desarrollo

# Instalar FRONTEND
npm i

# Correr APP FRONTEND
npm run start
