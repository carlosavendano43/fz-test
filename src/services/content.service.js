import axios from 'axios';

const api = 'http://localhost:4000/api/';     

const getAllPlaylists = async() => {
    const res = await axios.get(`${api}get-all-playlists`);
    return res.data;
}

const getAll = async() => {
    const res = await axios.get(`${api}get-all-content`);
    return res.data;
}

const getContentDetailService = async(id) => {
    const res = await axios.get(`${api}get-content-details/${id}`);
    return res.data;
}


export { 
    getAllPlaylists, 
    getAll, 
    getContentDetailService, 
}