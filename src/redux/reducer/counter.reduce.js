import { COUNTER_ADD, COUNTER_SET_VALUE, COUNTER_TOTAL }  from "../constants/counter.constants";


export const countersReducer = ( state = [], action ) => {
    switch ( action.type ) {
        case COUNTER_ADD:
          return [
            ...state,
            { id: action.counterId, value: 0 },
          ];
 
        case COUNTER_SET_VALUE:
            const s = state.map(x =>{
                if(x.id === action.counterId){
                    x.value = action.value;
                }
                return x;
            });
            return s;
        case COUNTER_TOTAL:
            const r = state.reduce((sum,valor) => ( sum + valor.value),0)
            return r;
        default:
            return state;
      
    }
}