import { CONTENT_GET_ALL, CONTENT_GET_DETAILS, CONTENT_CLEAR } from '../constants/content.constants';
const intialState = {
    playlist: [],
};
export const contentReducer = (state = intialState, { type, payload } ) => {
    switch(type){
        case CONTENT_GET_ALL:
            return {...state,playlist:payload};
        default: // need this for default case
            return state 
    }
}

export const contentReducerDetails = (state = {}, { type, payload }) => {
    switch(type){
        case CONTENT_GET_DETAILS:
            return {...state,...payload};
        case CONTENT_CLEAR:
            return {};
        default: // need this for default case
            return state 
    }
}