export const COUNTER_ADD = '@counter/add';
export const COUNTER_SET_VALUE = '@counter/setValue';
export const COUNTER_TOTAL = '@counter/total';