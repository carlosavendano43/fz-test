export const CONTENT_GET_ALL = '@content/getall';
export const CONTENT_GET_DETAILS = '@content/getdetails';
export const CONTENT_CLEAR = '@content/clear';