import {CONTENT_GET_ALL,CONTENT_GET_DETAILS, CONTENT_CLEAR} from '../constants/content.constants';
export const getContentALL = (playlists) =>{

    return {
        type:CONTENT_GET_ALL,
        payload:playlists
    }

}

export const getContentDetails  = (details) =>{
    return {
        type:CONTENT_GET_DETAILS,
        payload:details
    }
}

export const removeContent = () => {
    return {
      type: CONTENT_CLEAR,
    };
  };