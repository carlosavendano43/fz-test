import { COUNTER_ADD, COUNTER_SET_VALUE, COUNTER_TOTAL }  from "../constants/counter.constants";



export const addCounter = ( counterId ) => {
    return {
      type: COUNTER_ADD,
      counterId,
    };
}
  

export const  setCounterValue = ( counterId, value ) => {
    return {
      type: COUNTER_SET_VALUE,
      counterId,
      value,
    };
}

export const TotalCounter = () => {
  return {
    type: COUNTER_TOTAL,
  };
}