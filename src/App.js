
import { BrowserRouter as Router } from "react-router-dom";
import Header from './components/header/header';
import Footer from './components/footer/footer'
import Main from './components/main/main';
import 'bootstrap/scss/bootstrap.scss';
import './App.scss';

const App = () => {
  return (
    <>
      <Router>
        <Header/>
        <Main/>
        <Footer/>
      </Router>
    </>
  );
}

export default App;
