import { Routes, Route } from "react-router-dom";
////////////////// pages ////////////
import Content from '../../pages/content/content';
import ContenidoDetails from '../../pages/contentDetails/contentDetails'
import Counters from '../../pages/counters/counters';

const Main = () => {
    return(
    <main>
        <Routes>
          <Route path="/" element={<Content/>}></Route>
          <Route exact path="/contenidos" element={<Content/>}></Route>
          <Route exact path="/contenidos/detalle/:id" element={<ContenidoDetails/>}></Route>
          <Route exact path="/contadores" element={<Counters/>}></Route>
        </Routes>
    </main>
    )
}

export default Main;