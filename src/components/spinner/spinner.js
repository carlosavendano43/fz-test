import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
const Spinner = () =>{
    return(
        <div className="d-flex h-100 justify-content-center align-items-center">
            <Box sx={{ display: 'flex' }}>
                <CircularProgress />
            </Box>
        </div>
    )
}
export default Spinner;