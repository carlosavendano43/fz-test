import logo from '../../assets/img/logo.png';
import CallIcon from '@mui/icons-material/Call';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import EmailIcon from '@mui/icons-material/Email';
import Tooltip from '@mui/material/Tooltip';
import {
    Link
  } from "react-router-dom"
import './footer.scss';
const Footer = () =>{
    return(
        <footer className="footer">
            <div className="footer_navigation">
                <div className="footer_logo">
                    <Link to="/">
                        <img src={logo} alt="test"/>
                    </Link>
                </div>
                <div className="menu_footer_menu_container">
                    <ul className="navigation">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/contenidos">Contenidos</Link>
                        </li>
                        <li>
                            <Link to="/contadores">Contadores</Link>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="footer_contact">
                <div className="footer_icon_contact">
                    <a className="hover-none text-decoration-none">Contacto</a>
                    <Tooltip title="Teléfono" placement="top">
                        <a rel="noopener noreferrer" href="tel:+56936488729">
                            <CallIcon/>
                        </a>
                    </Tooltip>
                    <Tooltip title="Correo" placement="top">
                        <a rel="noopener noreferrer" href="mailto:carlosavendano43@gmail.com" target="_blank">
                            <EmailIcon/>
                        </a>
                    </Tooltip>
                    <Tooltip title="Linkedin" placement="top">
                        <a rel="noopener noreferrer" href="https://www.linkedin.com/in/carlos-jose-avenda%C3%B1o-mora-32096216a/" target="_blank">
                            <LinkedInIcon/>
                        </a>
                    </Tooltip>
                </div>
            </div>
        </footer>
    )
}
export default Footer;