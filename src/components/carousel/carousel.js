
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';
import { Link } from "react-router-dom"
// import Swiper core and required modules
import SwiperCore, {
    Keyboard,Autoplay,Pagination,Navigation
  } from 'swiper';

import 'swiper/swiper.scss';
import './carousel.scss';

SwiperCore.use([Keyboard,Autoplay,Pagination,Navigation]);

 const Carousel = ({datos}) =>{
    return(
        <>
            <div className="carousel-slider">
                <Swiper slidesPerView={1} spaceBetween={30} loop={true} pagination={{"clickable": true}} navigation={true} autoplay={{"delay": 2500,"disableOnInteraction": false}} keyboard={{"enabled": true}} className="mySwiper">
                    
                    {
                        datos.map((data,i)=>
                            <SwiperSlide key={i}>
                                <div className="carousel-slider-animate-opacity">
                                    <img src={`https://services.nunchee.tv/api/2.0/assets/images/${data.images[0]._id}/view/backdrop/70`} alt="test"/>
                                    <div className="slide-content">
                                        <h2>{data.title}</h2>
                                        <p>{data.description}</p>
                                        <Link className="btn btn-secondary mt-3 text-decoration-none text-white" to={`/contenidos/detalle/${data.id}`}>Ver Detalles</Link>
                                    </div>
                                </div>
                            </SwiperSlide>
                        )
                    }
                    
                </Swiper>
            </div>
        </>
    );
}
export default Carousel;