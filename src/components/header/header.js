import './header.scss';
import logo from '../../assets/img/logo.png';
import {
    Link
  } from "react-router-dom"
const Headers = () =>{
    return(
        <header className="header">
            <nav className="header_container">
           
                    <div className="header_logo">
                        <Link to="/">
                            <img src={logo} alt="test"/>
                        </Link>
                    </div>
                    <div className="header_navigate">
                        <ul className="navigation">
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/contenidos">Contenidos</Link>
                            </li>
                            <li>
                                <Link to="/contadores">Contadores</Link>
                            </li>
                        </ul>
                    </div>
 
            </nav>
        </header>
    )
}

export default Headers;