import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Tooltip from '@mui/material/Tooltip';
import { setCounterValue } from '../../redux/action/counter.action';
import { useDispatch } from 'react-redux'; 

const Counter = ({ datos }) => {
    const dispatch =  useDispatch();
    const onDecrease = () => {
        dispatch(setCounterValue(datos.id,datos.value-1));
    }

    const onIncrease = () => {
        dispatch(setCounterValue(datos.id,datos.value+1));
    }
  
    return(
        <div className="col-lg-4">
            <div className="m-5">
                <ButtonGroup>
                    <Tooltip title="Restar" placement="top">
                        <Button aria-label="reduce" onClick={ onDecrease }>
                            <RemoveIcon fontSize="small" />
                        </Button>
                    </Tooltip>
                    <Tooltip title="Sumar" placement="top">
                        <Button aria-label="increase" onClick={ onIncrease }>
                            <AddIcon fontSize="small" />
                        </Button>
                    </Tooltip>
                </ButtonGroup>
                <div className="px-4">{ datos.value }</div>
            </div>
        </div>
    );

}

export default Counter;