import { contentReducer, contentReducerDetails } from './redux/reducer/content.reduce';
import { countersReducer } from './redux/reducer/counter.reduce';
import { createStore, combineReducers } from 'redux';

const reducer = combineReducers({
    contentList: contentReducer,
    contentDetails: contentReducerDetails,
    counterList: countersReducer,
})
export const store = createStore(reducer,{});