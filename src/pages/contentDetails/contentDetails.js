import { useParams} from "react-router-dom";
import React, { useEffect } from 'react';
import { getContentDetailService } from '../../services/content.service';
import { getContentDetails, removeContent } from '../../redux/action/content.action';
import { useSelector, useDispatch } from 'react-redux'; 

import Spinner from '../../components/spinner/spinner';
import './contentDetails.scss';
const ContentDetails = ()  =>{
    let { id } = useParams();
    const content = useSelector((state) => state.contentDetails);

    const dispatch = useDispatch();
    const fetchData = async (id) => {
        const response = await getContentDetailService(id);
        dispatch(getContentDetails(response));
    }
    useEffect(() => {
        if(id && id !== "") fetchData(id);
        return () => {
            dispatch(removeContent());
        }
    },[id]);

    return(
        <>
                {
                    content ?
                    <div className="position-relative">
                        {
                            content.images && content.images.length > 0 ? 
                            <img className="w-100" src={`https://services.nunchee.tv/api/2.0/assets/images/${content.images[0]._id}/view/${content.images[0].type}/70`} alt="test"/>
                             :
                             <div className="h-vh-70">
                                <Spinner/>
                            </div>   
                        }
                        
                        <div className="slide-content">
                            <h2 className="text-white">{content.title}</h2>
                            <p className="text-white">{content.description}</p>
                        </div>
                    </div> :
                    <div className="h-vh-70">
                        <Spinner/>
                    </div>   
                }
        </>
    );
}

export default ContentDetails;