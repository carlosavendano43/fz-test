
import Button from '@mui/material/Button';
import { v4 as uuidv4 } from 'uuid';
import Counter from '../../components/counter/counter';
import { useSelector, useDispatch } from 'react-redux'; 
import {  addCounter } from '../../redux/action/counter.action';


const Counters = () => {
    const counterList = useSelector((state) => state.counterList);
    const dispatch =  useDispatch();
    const total = counterList.reduce((sum,valor) => ( sum + valor.value),0)

    const addCounters = () =>{
        let id = uuidv4();
        dispatch(addCounter(id));
    }

    return(
        <>

            <div >
                <div className="container p-5">
                   <div className="m-5">
                
                        <div className="row">
                            {
                                counterList.map((data,i)=>
                                    <Counter key={i} datos={data}/>
                                )
                            }
                        </div>
                        <div className="d-flex justify-content-between aling-items-center">
                            <Button variant="contained" onClick={() => addCounters() }>Agregar contador</Button>
                            <div>
                                <p className="text-black">Total:{total}</p>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </>
    );
}

export default Counters;