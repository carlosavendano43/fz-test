import Carousel from '../../components/carousel/carousel';
import React, { useEffect } from 'react';
import { getAll } from '../../services/content.service';
import { useSelector, useDispatch } from 'react-redux';
import './content.scss';
import { getContentALL } from '../../redux/action/content.action';
import Spinner from '../../components/spinner/spinner';

export default function Content() {
   
    const contentList = useSelector((state) => state.contentList);
    const dispatch = useDispatch();
    const fetchData = async () => {
        const response = await getAll();
        dispatch(getContentALL(response));
    }
    useEffect(() => {
        fetchData();
    },[]);

    return(
        <>
                {
                    contentList.playlist.length > 0 ?
                    <Carousel datos={contentList.playlist}/> :
                    <div className="h-vh-70">
                        <Spinner/>
                    </div>   
                }
        </>
    );
}